Booklist::Application.routes.draw do
  
  post '/rate' => 'rater#create', :as => 'rate'
  resources :messages
 
  get "pages/faq"
  get "pages/about" 
  devise_for :users
  resources :posts
   resources :users
  
  get "conversations/trash"

  get '/index' => 'posts#local'

 # get '/posts_path' => 'posts#gadgets'
 # get '/posts_path' => 'post#localgadgets'
  
   
    get '/gadgets' => 'posts#gadgets'
    get '/localgadgets'  => 'posts#localgadgets'
  
  get '/services' => 'posts#services'
  get '/localservices' => 'posts#localservices'
  
  get '/computers' => 'posts#computers'
  get '/localcomputers' => 'posts#localcomputers'
  
  get '/electronics' => 'posts#electronics'
  get '/localelectronics' => 'posts#localelectronics'
  
  get '/gear' => 'posts#gear'
  get '/localgear' => 'posts#localgear'
  
  get '/books' => 'posts#books'
  get '/localbooks' => 'posts#localbooks'
  
       
 
  
  
  # The priority is based upon order of creation: first created -> highest priority.
  
  #match '/:username' => "users#show", :via => :get  

  
  resources :comments, :only => [:create, :destroy]
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  root 'posts#index'
  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'
  
  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products
 
  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end
  
  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
  
 


  
  resources :messages do
  member do
    post :new
  end
end
resources :conversations do
  member do
    post :reply
    post :trash
    post :untrash
  end
 collection do
    get :trashbin
    post :empty_trash
 end
  
  
end
  


   
end