class Post < ActiveRecord::Base
  belongs_to :user
 
  
  
  validates :price, :numericality => {:only_integer => true}
  searchable do
    text :title
    text :author 
    text :course
    text :school
    text :email
    text :description
    text :price
    text :category
    integer :id
    time :updated_at
    
    
    
    string :school #notice, facet fields must be string, just add the field from above and diplicate it as string if text
   
  
   # ensure tha t a user_id is present
 # validates :user_id, presence: true
  end
  # ensure that title is present and at least 10 chars long
  validates :title, length: { minimum: 1 }, presence: true
   CATEGORY = ["BOOKS", "ELECTRONICS", "GADGETS", "COMPUTERS", "GEAR", "SERVICES"]
  
  
  
end
 