json.array!(@posts) do |post|
  json.extract! post, :id, :title, :author, :course, :school, :price, :description
  json.url post_url(post, format: :json)
end
