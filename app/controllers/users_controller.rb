class UsersController < ApplicationController
  def show
    @user = User.find(params[:id])
  
    #@user= User.find_by_username(params[:username])
    @comments = @user.comment_threads.order('created_at desc')
    @new_comment = Comment.build_from(@user, current_user, "")
  end   
  
  def create
     @user = User.create( user_params )
    
    
  end
 
  

    private

# Use strong_parameters for attribute whitelisting
# Be sure to update your create() and update() controller methods.

def user_params
  params.require(:user).permit(:avatar)
end
  
  
end      