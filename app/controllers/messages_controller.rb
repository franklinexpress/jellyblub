class MessagesController < ApplicationController
  
     
     def new
       @user = User.find_by_username(params[:user])
    @message = current_user.messages.new
       
       
  end
 
   # POST /message/create
  def create
    @recipient = User.find_by_username(params[:user])
    current_user.send_message(@recipient, params[:body], params[:subject])
    flash[:notice] = "Message has been sent!"
    redirect_to :conversations
  end

  
  
  
end
 