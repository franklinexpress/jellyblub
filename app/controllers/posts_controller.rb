class PostsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_post, only: [:show, :edit, :update, :destroy]
  
  


 

  # GET /posts
  # GET /posts.json
  def index
   
    @search = Post.search do #Post.search do
      fulltext params[:search]
      
      paginate :page => params[:page], :per_page => 20 
      order_by(:updated_at, :desc)
       end
    @posts = @search.results
  end

 
  
  
  def local
    
    @search = Post.search do #Post.search do
       fulltext params[:search]         
      with(:school, current_user.school)
      paginate :page => params[:page], :per_page => 20 
      order_by(:updated_at, :desc)
       end
       @posts = @search.results   
    render :index  
  end
  
  
  
  def gadgets
    
    @search = Post.search do #Post.search do
      fulltext params[:category]
      
      paginate :page => params[:page], :per_page => 20 
      order_by(:updated_at, :desc)
       end
       @posts = @search.results   
    render :index
  end
  
 
  def localgadgets
    @search = Post.search do #Post.search do
      fulltext params[:category] 
      with(:school, current_user.school)
      paginate :page => params[:page], :per_page => 20 
      order_by(:updated_at, :desc)
      end
       @posts = @search.results   
    render :index
  end    
  
  
  def services
    
    @search = Post.search do #Post.search do
      fulltext params[:category]
      
      paginate :page => params[:page], :per_page => 20 
      order_by(:updated_at, :desc)
       end
       @posts = @search.results   
    render :index
  end
  
 
  def localservices
    @search = Post.search do #Post.search do
      fulltext params[:category] 
      with(:school, current_user.school)
      paginate :page => params[:page], :per_page => 20 
      order_by(:updated_at, :desc)
      end
       @posts = @search.results   
    render :index
  end    
  
  
  def books
    
    @search = Post.search do #Post.search do
      fulltext params[:category]
      
      paginate :page => params[:page], :per_page => 20 
      order_by(:updated_at, :desc)
       end
       @posts = @search.results   
    render :index
  end
  
 
  def localbooks
    @search = Post.search do #Post.search do
      fulltext params[:category] 
      with(:school, current_user.school)
      paginate :page => params[:page], :per_page => 20 
      order_by(:updated_at, :desc)
      end
       @posts = @search.results   
    render :index
  end    
  
  
  def electronics
    
    @search = Post.search do #Post.search do
      fulltext params[:category]
      
      paginate :page => params[:page], :per_page => 20 
      order_by(:updated_at, :desc)
       end
       @posts = @search.results   
    render :index
  end
  
 
  def localelectronics
    @search = Post.search do #Post.search do
      fulltext params[:category] 
      with(:school, current_user.school)
      paginate :page => params[:page], :per_page => 20 
      order_by(:updated_at, :desc)
      end
       @posts = @search.results   
    render :index
  end    
  
  
  def gear
    
    @search = Post.search do #Post.search do
      fulltext params[:category]
      
      paginate :page => params[:page], :per_page => 20 
      order_by(:updated_at, :desc)
       end
       @posts = @search.results   
    render :index
  end
  
 
  def localgear
    @search = Post.search do #Post.search do
      fulltext params[:category] 
      with(:school, current_user.school)
      paginate :page => params[:page], :per_page => 20 
      order_by(:updated_at, :desc)
      end
       @posts = @search.results   
    render :index
  end    
      
  def computers
    
    @search = Post.search do #Post.search do
      fulltext params[:category]
      
      paginate :page => params[:page], :per_page => 20 
      order_by(:updated_at, :desc)
       end
       @posts = @search.results   
    render :index
  end
  
 
  def localcomputers
    @search = Post.search do #Post.search do
      fulltext params[:category] 
      with(:school, current_user.school)
      paginate :page => params[:page], :per_page => 20 
      order_by(:updated_at, :desc)
      end
       @posts = @search.results   
    render :index
  end    
  
 
  
  

  # GET /posts/1
  # GET /posts/1.json
  def show
  end
  
 
  
  
  # GET /posts/new
  def new
    @post = Post.new
  end

  # GET /posts/1/edit
  def edit
  end

  # POST /posts
  # POST /posts.json
  def create
   
    @post = Post.new(post_params) 
    @post.email = current_user.username
    @post.user_id = current_user.id
    respond_to do |format|
      if @post.save
        format.html { redirect_to @post, notice: 'Post was successfully created.' }
        format.json { render action: 'show', status: :created, location: @post }
      else
        format.html { render action: 'new' }
        format.json { render json: @post.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /posts/1
  # PATCH/PUT /posts/1.json
  def update
    respond_to do |format|
      if @post.update(post_params)
        format.html { redirect_to @post, notice: 'Post was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @post.errors, status: :unprocessable_entity }
      end
    end
  end

  
  
  # DELETE /posts/1
  # DELETE /posts/1.json
  def destroy
    
   if current_user.username == @post.email 
    @post.destroy
      respond_to do |format|
      format.html { redirect_to posts_url }
      format.json { head :no_content }
      end
   end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_post
      @post = Post.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def post_params
      params.require(:post).permit(:title, :author, :course, :school, :price, :category, :description)
    end
  
  
  
  
  
  

  
  
  
 
  
end
