class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.text :title
      t.string :author
      t.string :course
      t.string :school
      t.string :price
      t.text :description
      t.string :user_id

      t.timestamps
    end
  end
end
